package vn.edu.vnuk.jdbc.model;

public class Motorbike {
	private int id;
	private String brand;
	private String model;
	private int engineSize;
	private String gearbox;

	public Motorbike(String brand, String model, int engineSize, String gearbox) {
		this.brand = brand;
		this.model = model;
		this.engineSize = engineSize;
		this.gearbox = gearbox;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getEngineSize() {
		return engineSize;
	}

	public void setEngineSize(int engineSize) {
		this.engineSize = engineSize;
	}

	public String getGearbox() {
		return gearbox;
	}

	public void setGearbox(String gearbox) {
		this.gearbox = gearbox;
	}
	
}
