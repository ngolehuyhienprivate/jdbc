package vn.edu.vnuk.jdbc.sql;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import vn.edu.vnuk.jdbc.ConnectionFactory;

public class CreateMotorbikes {
	static Connection conn = null;
	static Statement stmt = null;
	   
	public static void main(String[] args) throws SQLException{
		
		Connection conn = new ConnectionFactory().getConnection();
		stmt = conn.createStatement();
		
		String sql = "CREATE TABLE motorbikes " +
                "(id BIGINT NOT NULL AUTO_INCREMENT, " +
                " branch VARCHAR(255), " + 
                " model VARCHAR(255), " + 
                " engine_size BIGINT, " + 
                " gearbox VARCHAR(255), " + 
                " PRIMARY KEY ( id ))"; 

		stmt.executeUpdate(sql);
		System.out.println("Create table successfully!");
	}
}
