package vn.edu.vnuk.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import vn.edu.vnuk.jdbc.model.Motorbike;

public class InsertMotorbike {

	public static void main(String[] args) throws SQLException{
		
		Connection connection = new ConnectionFactory().getConnection();
		
		Motorbike[] inputs = new Motorbike[3];
		
		inputs[0] = new Motorbike ("HONDA", "SH", 150, "Automatics");
		inputs[1] = new Motorbike ("YAMAHA", "Exciter", 135, "Manual");
		inputs[2] = new Motorbike ("SYM", "Angela", 50, "Semi-Auto");
		
		try{
			Statement st = connection.createStatement();
			for(Motorbike input : inputs) {
				String a = ("INSERT INTO motorbikes (branch, model, engine_size, gearbox) VALUES (" + 
						"'" + input.getBrand() + "'," + 
						"'" + input.getModel() + "'," + 
						input.getEngineSize() +
						",'" +input.getGearbox() + "'"+
						");");
				st.executeUpdate(a);
			}
			System.out.println(inputs.length + " rows affected");
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			connection.close();
			System.out.println("Done!");
		}
		
	}

}
