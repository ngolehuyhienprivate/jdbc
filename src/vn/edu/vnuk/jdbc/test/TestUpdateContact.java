package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;
import java.util.Calendar;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TestUpdateContact {

	public static void main(String[] args) {
		ContactDao contactDao = new ContactDao();
		try {
			Contact contact = new Contact();
			contact.setId(1L);
			contact.setName("Ngo Le Huy Hien");
			contact.setEmail("ngolehuyhien@gmail.com");
			contact.setAddress("DaNang, Vietnam");
			contact.setDateOfBirth(Calendar.getInstance());
			
			ContactDao contactTest = new ContactDao();
			contactTest.update(contact);
			
			Contact list = contactDao.read(1);
			
			System.out.println("---------UPDATED DATA---------");
			System.out.println("Id: " + list.getId());
			System.out.println("Name: " +list.getName());
			System.out.println("Email: " +list.getEmail());
			System.out.println("Address: " +list.getAddress());
			System.out.println("Date Of Birth: " +list.getDateOfBirth().getTime().toString());
			System.out.println("-------------------------------");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
