package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TestReadSingleContact {

	public static void main(String[] args) {
		ContactDao contactDao = new ContactDao();
		try {
			Contact list = contactDao.read(1);
						
			System.out.println("Id: " + list.getId());
			System.out.println("Name: " +list.getName());
			System.out.println("Email: " +list.getEmail());
			System.out.println("Address: " +list.getAddress());
			System.out.println("Date Of Birth: " +list.getDateOfBirth().getTime().toString());
			System.out.println("-------------------------------");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
