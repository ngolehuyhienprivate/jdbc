package vn.edu.vnuk.jdbc.test;

import java.sql.SQLException;
import java.util.List;

import vn.edu.vnuk.jdbc.dao.ContactDao;
import vn.edu.vnuk.jdbc.model.Contact;

public class TestReadContacts {

	public static void main(String[] args) {
		ContactDao contactDao = new ContactDao();
		try {
			List<Contact> lists = contactDao.read();
			
			for(Contact list : lists) {
				System.out.println("Id: " + list.getId());
				System.out.println("Name: " +list.getName());
				System.out.println("Email: " +list.getEmail());
				System.out.println("Address: " +list.getAddress());
				System.out.println("Date Of Birth: " +list.getDateOfBirth().getTime().toString());
				System.out.println("-------------------------------");
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
